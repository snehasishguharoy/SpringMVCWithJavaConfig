/**
 * 
 */
package spitter.web;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.servlet.view.InternalResourceView;
import spitter.config.SpitterConfig;
import spittr.Spittles;
import spittr.impl.SpiitleRepositoryImpl;

/**
 * @author sony
 *
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { SpitterConfig.class })
public class HomeControllerTest {

	// @Test
	// @Ignore
	// public void testHomeController() throws Exception {
	// HomeController controller = new HomeController();
	// MockMvc mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
	//
	// mockMvc.perform(get("/abc")).andExpect(view().name("home"));
	//
	// }
	@Test
	public void testRecentSpittles() throws Exception {
		List<Spittles> spittles = SpiitleRepositoryImpl.createSpiitles(20);
		SpiitleRepositoryImpl impl = Mockito.mock(SpiitleRepositoryImpl.class);
		Mockito.when(impl.findSpittles(Long.MAX_VALUE, 20)).thenReturn(spittles);
		HomeController controller = new HomeController(impl);
		MockMvc mockMvc = MockMvcBuilders.standaloneSetup(controller)
				.setSingleView(new InternalResourceView("/WEB-INF/views/spittles.jsp")).build();
		mockMvc.perform(get("/abc/spittles")).andExpect(view().name("spittles"))
				.andExpect(model().attributeExists("spittleList"));

	}

	@Test
	public void testgetSpittlesWithQueryParam() throws Exception {
		List<Spittles> spittles = SpiitleRepositoryImpl.createSpiitles(2, 10);
		SpiitleRepositoryImpl impl = Mockito.mock(SpiitleRepositoryImpl.class);
		Mockito.when(impl.findSpittles(5, 10)).thenReturn(spittles);
		HomeController controller = new HomeController(impl);
		MockMvc mockMvc = MockMvcBuilders.standaloneSetup(controller)
				.setSingleView(new InternalResourceView("/WEB-INF/views/spittles.jsp")).build();

		mockMvc.perform(get("/xyz/spittles?max=5&count=30")).andExpect(view().name("spittles"))
				.andExpect(model().attributeExists("spittleList"));
	}

	@Test
	public void testgetSpittlesWithPathParam() throws Exception {
		Spittles spittles = new Spittles("Spittle100", new Date());
		SpiitleRepositoryImpl impl = Mockito.mock(SpiitleRepositoryImpl.class);
		Mockito.when(impl.getSpittleByMessage("Spittle101")).thenReturn(spittles);
		HomeController controller = new HomeController(impl);
		MockMvc mockMvc = MockMvcBuilders.standaloneSetup(controller)
				.setSingleView(new InternalResourceView("/WEB-INF/views/spittle.jsp")).build();

		mockMvc.perform(get("/abc/Spittle1")).andExpect(view().name("spittle"))
				.andExpect(model().attributeExists("spittle"));
	}

}
