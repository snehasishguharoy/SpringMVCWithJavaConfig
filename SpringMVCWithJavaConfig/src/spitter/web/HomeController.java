/**
 * 
 */
package spitter.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import spittr.SpittleRepository;

@Controller
@RequestMapping(value = { "/abc", "/xyz" })
public class HomeController {

	private SpittleRepository spittleRepository;

	@Autowired
	public HomeController(SpittleRepository spittleRepository) {
		this.spittleRepository = spittleRepository;
	}

	@RequestMapping(method = RequestMethod.GET)
	public String home() {
		return "home";
	}
	
	
	 @RequestMapping(value = "/spittles", method = RequestMethod.GET)
		 public String getSpittles(Model model) {
		
		model.addAttribute(spittleRepository.findSpittles(1,10));
		 return "spittles";
		 
		 
		 }

	// @RequestMapping(value = "/spittles", method = RequestMethod.GET)
	// public String getSpittles(Map model) {
	//
	// model.put("spittleList", spittleRepository.findSpittles(Long.MAX_VALUE,
	// 5));
	// return "spittles";
	// }

	// @RequestMapping(value = "/spittles", method = RequestMethod.GET)
	// public @ResponseBody List<Spittles> getSpittles(Map model) {
	// List<Spittles> spittleList =
	// spittleRepository.findSpittles(Long.MAX_VALUE, 20);
	//
	// return spittleList;
	//
	// }
	// Using Query Parameters
//	@RequestMapping(value = "/spittles", method = RequestMethod.GET)
//	public ModelAndView getSpittles(@RequestParam(value = "max", defaultValue = "20") Long max,
//			@RequestParam(value = "count", defaultValue = "10") int count, ModelAndView modelAndView) {
//
//		List<Spittles> result = spittleRepository.findSpittles(max, count);
//		modelAndView.setViewName("spittles");
//		return modelAndView.addObject("spittleList", result);
//
//	}
	@RequestMapping(value="/{message}")
	public String findSpittle(@PathVariable String message,Model model){
		model.addAttribute("spittle",spittleRepository.getSpittleByMessage(message));
		return "spittle";
		
	}
}
