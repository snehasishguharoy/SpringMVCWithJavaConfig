/**
 * 
 */
package spitter.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import spittr.Spitter;
import spittr.SpittleRepository;

/**
 * @author sony
 *
 */
@Controller
@RequestMapping("/spitter")
public class UserController {

	private SpittleRepository spittleRepository;

	@Autowired
	public UserController(SpittleRepository spittleRepository) {
		this.spittleRepository = spittleRepository;
	}

	@RequestMapping(value = "/register", method = RequestMethod.GET)
	public String registerForm() {

		return "registrationForm";
	}

	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public String registerForm(Spitter spitter) {

		spittleRepository.saveSpitter(spitter);

		return "redirect:/spitter/" + spitter.getFirstName();
	}

	@RequestMapping(value = "{userName}", method = RequestMethod.GET)
	public String showUserProfile(@PathVariable String userName, Model model) {

		Spitter spitter = spittleRepository.getSpitters(userName);
		model.addAttribute(spitter);
		return "profile";

	}
}
