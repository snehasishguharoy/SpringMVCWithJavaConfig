/**
 * 
 */
package spitter.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.web.accept.ContentNegotiationManager;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.ContentNegotiationConfigurer;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.ContentNegotiatingViewResolver;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

import spitter.web.HomeController;
import spittr.impl.SpiitleRepositoryImpl;

/**
 * @author sony
 *
 */
@Configuration
@ComponentScan(basePackageClasses = { HomeController.class, SpiitleRepositoryImpl.class })
@EnableWebMvc
public class WebConfig extends WebMvcConfigurerAdapter {

	 @Bean
	 public ViewResolver viewResolver() {
	
	 InternalResourceViewResolver internalResourceViewResolver = new
	 InternalResourceViewResolver();
	 internalResourceViewResolver.setPrefix("/WEB-INF/views/");
	 internalResourceViewResolver.setSuffix(".jsp");
	 internalResourceViewResolver.setExposeContextBeansAsAttributes(true);
	 return internalResourceViewResolver;
	 }
//	@Bean
//	public ViewResolver cnViewResolver(ContentNegotiationManager cm) {
//		ContentNegotiatingViewResolver contentNegotiatingViewResolver = new ContentNegotiatingViewResolver();
//		contentNegotiatingViewResolver.setContentNegotiationManager(cm);
//		return contentNegotiatingViewResolver;
//
//	}
//
//	public void configureContentNegotiation(ContentNegotiationConfigurer configurer) {
//		configurer.defaultContentType(MediaType.APPLICATION_JSON);
//	}

	public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
		configurer.enable();
	}

}
