/**
 * 
 */
package spittr;

/**
 * @author sony
 *
 */
public interface SpittleRepository {

	java.util.List<Spittles> findSpittles(long max, int count);

	Spittles getSpittleByMessage(String message);

	void saveSpitter(Spitter spitter);
	
	Spitter getSpitters(String firtName);
	

}
