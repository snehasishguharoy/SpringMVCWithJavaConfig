/**
 * 
 */
package spittr.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import spittr.Spitter;
import spittr.SpittleRepository;
import spittr.Spittles;

/**
 * @author sony
 *
 */
@Component
public class SpiitleRepositoryImpl implements SpittleRepository {

	private static List<Spittles> list = new ArrayList<>();
	private static List<Spitter> spitters = new ArrayList<>();

	@Override
	public List<Spittles> findSpittles(long max, int count) {
		return createSpiitles(count);
	}

	public static List<Spittles> createSpiitles(int count) {

		for (int i = 0; i < count; i++) {
			list.add(new Spittles("Spittle" + i, new Date()));
		}
		return list;
	}

	public static List<Spittles> createSpiitles(long max, int count) {

		for (long i = max; i < count; i++) {
			list.add(new Spittles("Spittle" + i, new Date()));
		}
		return list;
	}

	public Spittles getSpittleByMessage(String message) {

		for (int i = 0; i < 10; i++) {
			list.add(new Spittles("Spittle" + i, new Date()));
		}
		return list.stream().filter(s -> s.getMessage().equals(message)).findAny().get();
	}

	@Override
	public void saveSpitter(Spitter spitter) {

		spitters.add(spitter);

	}

	@Override
	public Spitter getSpitters(String firtName) {

		return (Spitter) spitters.stream().filter(s -> s.getFirstName().equals(firtName)).findFirst().get();

	}

}
