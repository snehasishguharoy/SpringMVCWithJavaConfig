<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/style.css"/>">
</head>
<body>
	<h1 style="background-color: yellow">Welcome to the Spitter
		Application</h1>
	<a href="<c:url value="/spittles" />">Spittles</a>
	<br/>
	<a href="<c:url value="/spitter/register" />">Register</a>
</body>
</html>